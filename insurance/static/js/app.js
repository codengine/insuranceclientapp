Vue.component('risk_form', {
    template: '#id_form_template',
    props : ['form_fields'],
    data : function(){
        return {

        }
    }
});
Vue.component('form_input', {
    template: '#id_form_input_template',
    props : ['form_element'],
    data : function(){
        return {

        }
    }
});

String.prototype.capitalize = function () {
    return this.toLowerCase().split(' ').map(function (i) {
        if (i.length > 1) {
            return i.charAt(0).toUpperCase() + i.substr(1);
        } else {
            return i;
        }
    }).join(' ');
};

var gform_data = {};

var app = new Vue ({
    el: '#id_app',
    data: {
        page_title: "Insurance Management",
        page_heading: "Insurance Management",
        pageHeading: "page-heading",
        api_url_domain: "http://ec2-13-58-171-72.us-east-2.compute.amazonaws.com/api/",
        content_area: "list", //create or list
        selected_risk_type: "",
        risk_types: [],
        form_title: "",
        risk_item_headers: [],
        risk_items: [],
        loading: true,
        risk_fields: {},
        form_data: {},
        post_url: "",
        confirm_dialog_visible: false,
        confirm_dialog_message: "",
        search_keyword: ""
    },
    methods:{
        call_ajax: function(url, method, data, success_callback, error_callback) {
            if(method == "GET") {
                axios.get(url).then(success_callback, error_callback);
            }
            else if(method == "POST"){
                axios.post(url, data).then(success_callback).catch(error_callback);
            }
        },
        handle_confirm_dialog_close: function () {

        },
        get_risk_types: function(callback){
                //console.log(this);
            this.risk_types = [];
            var $this = this;
            var url = this.api_url_domain + "risk-types/";
            this.call_ajax(url, "GET", {}, function(response){
                results = response.data.results;
                for(var i = 0 ; i < results.length; i++) {
                    var item = results[i];
                    $this.risk_types.push({ id: item.id, name: item.name });
                }
                if(typeof callback == "function") {
                    callback();
                }
            }, function(error){
                console.log(error.statusText);
            });
            return false;
        },
        handle_risk_type_click: function (content_area) {
            this.content_area = content_area;
            this.loading = true;
            var $this = this;
            this.get_risk_types($this.handle_risk_type_select);
        },
        handle_search_by_keyword: function () {
            this.load_risk_list();
        },
        load_risk_list: function () {
            this.risk_items = [];
            this.risk_item_headers = [];
            var $this = this;
            var url = this.api_url_domain + "risk-list/"+ this.selected_risk_type +"/";
            if(this.search_keyword != "") {
                url = url + "?q=" + this.search_keyword;
            }
            this.call_ajax(url, "GET", {}, function(response){
                results = response.data.results;
                var headers_added = false;
                for(var i = 0 ; i < results.length; i++) {
                    var item = results[i];
                    if(!headers_added) {
                        for(var j in item) {
                            var header_verbose = $this.make_verbose(j);
                            $this.risk_item_headers.push(header_verbose);
                            headers_added = true;
                        }
                    }
                    $this.risk_items.push(item);
                }
            }, function(error){
                //console.log(error.statusText);
            });
            return false;
        },
        load_risk_fields: function () {
            this.form_data = {};
            gform_data = {};
            this.risk_fields = {};
            var $this = this;
            var url = this.api_url_domain + "risk-fields/"+ this.selected_risk_type +"/";
            this.call_ajax(url, "GET", {}, function(response){
                var results = response.data.results;
                for(var i = 0 ; i < results.length; i++) {
                    var risk_field = results[i];
                    //$this.risk_fields.push(risk_field);
                    $this.form_data[risk_field.field_name] = { value: "" };
                    var rendered_field = $this.render_risk_field(risk_field);
                    gform_data[risk_field.field_name] = rendered_field;
                }
                $this.form_data = gform_data;
                $this.risk_fields = gform_data;
            }, function(error){

            });
        },
        find_risk_type_text: function (risk_type_id) {
            risk_type_text = "";
            if(risk_type_id != "") {
                risk_type_id = parseInt(risk_type_id);
            }
            for(var i = 0; i < this.risk_types.length; i++ ) {
                if(this.risk_types[i].id == risk_type_id) {
                    risk_type_text = this.risk_types[i].name;
                }
            }
            return risk_type_text;
        },
        handle_risk_type_select: function (e) {
            this.loading = false;
            if(this.selected_risk_type != "") {
                if(this.content_area == "create") {
                    this.load_risk_fields();
                }
                else if(this.content_area == "list") {
                    return this.load_risk_list();
                }
                this.form_title = "Create " + this.find_risk_type_text(this.selected_risk_type);
            }
        },
        make_verbose: function (name) {
            var field_name_split = name.split("_");
            var name_verbose = "";
            for(var i = 0 ; i < field_name_split.length; i++ ) {
                name_verbose += field_name_split[i].capitalize() + " ";
            }
            return name_verbose;
        },
        get_choices: function (choice_string) {
            return choice_string.split(",");
        },
        get_widget_name: function (field_type) {
             if(field_type == "text") {
                 return "input";
             }
             else if(field_type == "long_text") {
                 return "textarea";
             }
             else if(field_type == "int") {
                return "input_number";
             }
             else if(field_type == "float") {
                return "input_number";
             }
             else if(field_type == "bool") {
                return "checkbox";
             }
             else if(field_type == "enum") {
                return "select";
             }
             else if(field_type == "date") {
                return "date";
             }
             else if(field_type == "datetime") {
                return "datetime";
             }
        },
        get_widget_type: function (field_type) {
            if(field_type == "text") {
                 return "text";
             }
             else if(field_type == "long_text") {
                 return "";
             }
             else if(field_type == "int") {
                return "number";
             }
             else if(field_type == "float") {
                return "number";
             }
             else if(field_type == "bool") {
                return "checkbox";
             }
             else if(field_type == "enum") {
                return "";
             }
             else if(field_type == "date") {
                return "text";
             }
             else if(field_type == "datetime") {
                return "text";
             }
        },
        render_risk_field: function (field) {
            var field_object = {};
            field_object["id"] = field.id;
            field_object["name"] = field.field_name;
            field_object["model"] = "";
            field_object["label"] = this.make_verbose(field.field_name);
            field_object["type"] = this.get_widget_type(field.field_type);
            field_object["required"] = field.is_required;
            field_object["options"] = this.get_choices(field.field_choices);
            field_object["widget"] = this.get_widget_name(field.field_type);
            return field_object;
        },
        format_post_parameters: function () {
              var post_data = {};
              for(field_name in this.form_data) {
                  post_data[field_name] = this.form_data[field_name]["model"];
              }
              return post_data;
        },
        submit_risk_form: function() {
            var $this = this;
            var url = this.api_url_domain + "risk-create/"+ this.selected_risk_type +"/";
            //console.log($this.form_data);
            var post_data = this.format_post_parameters();
            console.log(post_data);
            this.call_ajax(url, "POST", post_data, function(response){
                //response.data
                //alert(response.data);
                $this.confirm_dialog_visible = true;
                $this.confirm_dialog_message = "Item Saved Successfully";
            }, function(error){
                console.log(error.statusText);
                $this.confirm_dialog_message = "Item Save Failed";
            });
            return false;
        }
    },
    mounted: function () {
        this.get_risk_types();
    }
});
